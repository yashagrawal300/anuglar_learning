import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ApiserviceService {
  apiData:any;
  sortedData:any;
  currentSorting: string = "Sorted By Name";
  shuffled: string = "Recived Data";

  namesorting(a:any, b:any) {
    if (a.name < b.name) return -1;
    if (a.name > b.name) return 1;
    return 0;
  }

  changeByName(){
    this.sortedData = JSON.parse(JSON.stringify(this.apiData));
    this.sortedData.sort(this.namesorting);
    this.currentSorting = "Sorted By Name";


  }
  agesorting(d1:any,d2:any):number{
    var dt1= new Date(d1.dob.slice(6)+"/"+d1.dob.slice(3,5)+"/"+d1.dob.slice(0,2));
    var dt2= new Date(d2.dob.slice(6)+"/"+d2.dob.slice(3,5)+"/"+d2.dob.slice(0,2));
    if(dt1<=dt2){
      return 1;
    }else{
      return -1;
    }

    //dd-mm-yyyy to ---->>>> yyyy-mm-dd
  }

  changeByAge(){
    this.sortedData = JSON.parse(JSON.stringify(this.apiData));
    this.sortedData.sort(this.agesorting);
    this.currentSorting = "Sorted By Age"
  }
  constructor(private http:HttpClient) { }


  getData(){
    let url: string= "https://bitbucket.org/yashagrawal300/anuglar_learning/raw/f1700dba8da42a79609f1d028a536029a2651f09/birthday_sort/src/assets/data.json"

    return this.http.get(url);



    
  }

}
