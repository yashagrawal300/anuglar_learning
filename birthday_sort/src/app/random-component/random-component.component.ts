import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-random-component',
  templateUrl: './random-component.component.html',
  styleUrls: ['./random-component.component.css']
})
export class RandomComponentComponent implements OnInit {

  @Output() emitEvent: EventEmitter<any> = new EventEmitter<any>();
  constructor() { }

  onClick(){
    this.emitEvent.emit();
    
  }

  ngOnInit(): void {
  }

}
