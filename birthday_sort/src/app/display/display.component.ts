import { Component, OnInit } from '@angular/core';
import { ApiserviceService } from '../apiservice.service';

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.css']
})
export class DisplayComponent implements OnInit {
  sortByName:any;
  sortByAge: any;

  namesorting(a:any, b:any) {
    if (a.name < b.name) return -1;
    if (a.name > b.name) return 1;
    return 0;
  }

  constructor(public apiservice: ApiserviceService) { }
  changeByName(){
    this.sortByName = this.apiservice.apiData;
    this.sortByName.sort(this.namesorting);
  
    
  }

  ngOnInit(): void {
  
  }

}
