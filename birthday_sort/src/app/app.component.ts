import { Component } from '@angular/core';
import { ApiserviceService } from './apiservice.service';
import { DisplayComponent } from './display/display.component';
//api url of birthday data 
//https://bitbucket.org/yashagrawal300/anuglar_learning/raw/f1700dba8da42a79609f1d028a536029a2651f09/birthday_sort/src/assets/data.json
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'birthday_sort';

  parentCall(data:any): void{
    this.apiservice.shuffled = "Shuffled data";
    this.apiservice.apiData.sort( ()=>Math.random()-0.5 );
  }

  constructor(public apiservice: ApiserviceService) {};




}
