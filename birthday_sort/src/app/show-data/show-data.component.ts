import { Component, OnInit } from '@angular/core';
import { ApiserviceService } from '../apiservice.service';
@Component({
  selector: 'app-show-data',
  templateUrl: './show-data.component.html',
  styleUrls: ['./show-data.component.css']
})
export class ShowDataComponent implements OnInit {

  constructor(public apiservice:ApiserviceService) { }
  


  ngOnInit(): void {
    this.apiservice.getData().subscribe(data=>{
      this.apiservice.apiData = data;
      this.apiservice.changeByName();

    });

  
  }

}
