import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DisplayComponent } from './display/display.component';
import {HttpClientModule} from "@angular/common/http";
import { ShowDataComponent } from './show-data/show-data.component';
import { RandomComponentComponent } from './random-component/random-component.component';
@NgModule({
  declarations: [
    AppComponent,
    DisplayComponent,
    ShowDataComponent,
    RandomComponentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
