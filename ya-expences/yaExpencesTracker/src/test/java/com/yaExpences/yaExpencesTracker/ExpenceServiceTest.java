package com.yaExpences.yaExpencesTracker;
import com.yaExpences.yaExpencesTracker.repository.ExpenceRepository;
import com.yaExpences.yaExpencesTracker.service.ExpenceService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@RunWith(MockitoJUnitRunner.class)
public class ExpenceServiceTest {



    @InjectMocks
    ExpenceService expenceService;

    @Mock
    ExpenceRepository expenceRepository;


    @Test
    public void testgetExpenceBetweenDates() throws ParseException {
        String sDate1="2022-02-01";
        String sDate2="2022-02-09";
        Date date1=new SimpleDateFormat("yyyy-MM-dd").parse(sDate1);
        Date date2=new SimpleDateFormat("yyyy-MM-dd").parse(sDate2);

        Assert.assertNotEquals("",  expenceService.getExpenceBetweenDates(date1, date2).getClass().getSimpleName());

    }


}
