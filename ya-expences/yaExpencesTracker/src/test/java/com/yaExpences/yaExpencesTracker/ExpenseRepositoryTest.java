package com.yaExpences.yaExpencesTracker;


import com.yaExpences.yaExpencesTracker.model.ExpenceModel;
import com.yaExpences.yaExpencesTracker.repository.ExpenceRepository;
import com.yaExpences.yaExpencesTracker.service.ExpenceService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class ExpenseRepositoryTest {

    @Mock
    ExpenceRepository expenceRepository;

    @InjectMocks
    ExpenceService expenceService;

    @InjectMocks
    ExpenceModel expenceModel;
    @Test
    public void testExpense() throws ParseException {
        String sDate1="01/02/2022";
        String sDate2="03/02/2022";
        Date date1=new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);
        Date date2=new SimpleDateFormat("dd/MM/yyyy").parse(sDate2);

        Assert.assertNotEquals("",  expenceRepository.findAllExpenceByDate(date1, date2).getClass().getSimpleName());


    }

}
