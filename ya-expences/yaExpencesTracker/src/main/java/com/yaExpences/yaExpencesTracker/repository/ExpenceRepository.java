package com.yaExpences.yaExpencesTracker.repository;

import com.yaExpences.yaExpencesTracker.model.ExpenceModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ExpenceRepository extends JpaRepository<ExpenceModel, Integer> {


    @Query(value = "SELECT * FROM expences.expences WHERE exdate BETWEEN :d1 and :d2", nativeQuery = true)
    List<ExpenceModel> findAllExpenceByDate(Date d1, Date d2);
}
