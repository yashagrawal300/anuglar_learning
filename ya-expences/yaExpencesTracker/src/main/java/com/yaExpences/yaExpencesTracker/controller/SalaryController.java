package com.yaExpences.yaExpencesTracker.controller;

import com.yaExpences.yaExpencesTracker.Exceptions.ParameterMissingInRequestBody;
import com.yaExpences.yaExpencesTracker.model.SalaryModel;
import com.yaExpences.yaExpencesTracker.service.SalaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SalaryController {


    @Autowired
    SalaryService salaryService;



    @PutMapping("/api/updateSalary")
    public ResponseEntity<String> updateSalary(@RequestBody SalaryModel salaryModel){
        try{
            if(salaryModel.getId() == 0){
                throw new ParameterMissingInRequestBody("Id Required");
            }
            else{
                return new ResponseEntity( salaryService.updateSalary(salaryModel.getId(), salaryModel.getMoneyLeft()), HttpStatus.OK);
            }

        }
        catch (ParameterMissingInRequestBody ex){
            return new ResponseEntity(ex, HttpStatus.BAD_REQUEST);
        }



    }

    @GetMapping("/api/getCurrentMonthSalary")
    public SalaryModel getSalary(){
        return salaryService.getSalary();

    }

    @GetMapping("/api/getAllSalary")
    public List<SalaryModel> getAllSalary(){
        return salaryService.getAllSalary();
    }



}
