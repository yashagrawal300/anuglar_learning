package com.yaExpences.yaExpencesTracker.service;

import com.yaExpences.yaExpencesTracker.model.SalaryModel;
import com.yaExpences.yaExpencesTracker.repository.SalaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class SalaryService {


    @Autowired
    SalaryRepository salaryRepository;

    public boolean updateSalary(int index, int salary){
        try {
            SalaryModel s = salaryRepository.getById(index);
            s.setMoneyLeft(salary);
            salaryRepository.save(s);
            return true;
        }
        catch (Exception e){
            System.out.println(e);
            return false;
        }
    }


    public SalaryModel getSalary() {
        return salaryRepository.findById((int)salaryRepository.count());

    }

    public List<SalaryModel> getAllSalary(){
        return salaryRepository.findAll();
    }


}
