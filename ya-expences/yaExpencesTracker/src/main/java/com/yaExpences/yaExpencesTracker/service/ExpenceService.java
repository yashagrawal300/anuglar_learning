package com.yaExpences.yaExpencesTracker.service;

import com.yaExpences.yaExpencesTracker.model.ExpenceModel;
import com.yaExpences.yaExpencesTracker.model.SalaryModel;
import com.yaExpences.yaExpencesTracker.repository.ExpenceRepository;
import com.yaExpences.yaExpencesTracker.repository.SalaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class ExpenceService {

    @Autowired
    ExpenceRepository expenceRepository;

    @Autowired
    SalaryRepository salaryRepository;

    private final static Logger LOGGER =
            Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);



    @Cacheable(cacheNames = "ExpenceBetweenDates", key = "#d1.toString() + #d2.toString()") //Adding cache with the key value
    public List<ExpenceModel> getExpenceBetweenDates(Date d1, Date d2){
       return expenceRepository.findAllExpenceByDate(d1, d2);
    }



    @CacheEvict(value = "ExpenceBetweenDates", allEntries = true)  // Clearing all caches
    public ExpenceModel addExpense(ExpenceModel e) throws ParseException{
        LOGGER.log(Level.INFO, "My first Log Message");



        //Date formatting
        SimpleDateFormat onlyDate = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat onlyTime = new SimpleDateFormat("hh.mm aa");

        //Changing time zone and parsing string to date
        onlyDate.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
        onlyTime.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));

        Date date = new Date();
        String[] dat = onlyDate.format(date).split("/");
        String sDate1 = dat[2] + "/" + dat[1]+"/"+dat[0];
        Date date1=new SimpleDateFormat("yyyy/MM/dd").parse(sDate1);


        //setting variables
        e.setIndex(1+ (int)expenceRepository.count());
        e.setExpence(onlyTime.format(date)+" " +e.getExpence());
        e.setExdate(date1);

        SalaryModel s = salaryRepository.getById((int)salaryRepository.count());
        s.setMoneyLeft(s.getMoneyLeft() - e.getSpent());
        s.setSpentAmount(s.getSpentAmount() + e.getSpent());
        salaryRepository.save(s);
        expenceRepository.save(e);
        return e;
    }

}
