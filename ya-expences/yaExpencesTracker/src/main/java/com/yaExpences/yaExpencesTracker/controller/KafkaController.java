package com.yaExpences.yaExpencesTracker.controller;

import com.yaExpences.yaExpencesTracker.model.KafkaModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("kafka")
public class KafkaController {

    @Autowired
    KafkaTemplate<String, KafkaModel> kafkaTemplate;

    @Autowired
    KafkaTemplate<String, String> stringKafkaTemplate;

    private static final String TOPIC_json = "kafka-example-json"; //decide the topic(just like database name)
    private static final String TOPIC_string = "kafka-example";


    @GetMapping("/publish/{message}")
    public ResponseEntity publishMessageString(@PathVariable("message") final String message){
        stringKafkaTemplate.send(TOPIC_string, message);
        return new ResponseEntity(message, HttpStatus.OK);
    }



    @GetMapping("/publish/{number}/{message}")
    public ResponseEntity publishMessageString(@PathVariable("number") final int number, @PathVariable("message") final String message){
        KafkaModel kafkaModel = new KafkaModel(number, message);
        kafkaTemplate.send(TOPIC_json, kafkaModel);
        return new ResponseEntity(kafkaModel, HttpStatus.OK);
    }

}
