package com.yaExpences.yaExpencesTracker;

import com.yaExpences.yaExpencesTracker.model.SalaryModel;
import com.yaExpences.yaExpencesTracker.repository.SalaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.aws.autoconfigure.context.ContextRegionProviderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextStackAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Date;

@SpringBootApplication (exclude = {ContextStackAutoConfiguration.class, ContextRegionProviderAutoConfiguration.class})
@EnableCaching
public class YaExpencesTrackerApplication {

	public static void main(String[] args) {
		SpringApplication.run(YaExpencesTrackerApplication.class, args);
	}

	@Autowired
	SalaryRepository salaryRepository;



	@Scheduled(cron = "0 0 0 1 * *")
	void addNewSalary(){
		SalaryModel salaryModel = new SalaryModel();
		salaryModel.setSpentAmount(0);
		salaryModel.setMoneyLeft(50000);
		salaryModel.setDate(new Date());
		salaryModel.setId((int)salaryRepository.count() + 1);
		salaryRepository.save(salaryModel);
		System.out.println("Done");
	}

}


@Configuration
@EnableScheduling
@ConditionalOnProperty(name = "scheduling.enabled", matchIfMissing = true)
class SchedulingConfiguration{

}
