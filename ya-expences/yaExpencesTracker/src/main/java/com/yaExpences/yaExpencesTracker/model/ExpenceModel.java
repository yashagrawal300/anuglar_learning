package com.yaExpences.yaExpencesTracker.model;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "expences")
public class ExpenceModel {
    @Id
    private int index;
    private String expence;
    private int spent;
    private Date exdate;

    public ExpenceModel() {
    }

    public ExpenceModel(int index, String expence, int spend, Date exdate) {
        this.index = index;
        this.expence = expence;
        this.spent = spend;
        this.exdate = exdate;
    }

    public String getExpence() {
        return expence;
    }

    public void setExpence(String expence) {
        this.expence = expence;
    }

    public int getSpent() {
        return spent;
    }

    public void setSpent(int spend) {
        this.spent = spend;
    }

    public Date getExdate() {
        return exdate;
    }

    public void setExdate(Date exdate) {
        this.exdate = exdate;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
