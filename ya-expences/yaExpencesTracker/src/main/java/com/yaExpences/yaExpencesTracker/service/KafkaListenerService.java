package com.yaExpences.yaExpencesTracker.service;

import com.yaExpences.yaExpencesTracker.model.KafkaModel;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaListenerService {


    @KafkaListener(topics = "kafka-example", groupId = "group_id", containerFactory = "concurrentKafkaListenerContainerFactory")
    public void consume(String message){
        System.out.println("from consumer "+message);
    }

    @KafkaListener(topics = "kafka-example-json", groupId = "group_json", containerFactory = "kafkaModelListenerFactory")
    public void consumeJSON(KafkaModel kafkaModel){
         System.out.println("from consumer  " + kafkaModel.toString());
    }


}
