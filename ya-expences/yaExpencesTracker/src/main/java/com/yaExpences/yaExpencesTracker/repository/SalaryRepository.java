package com.yaExpences.yaExpencesTracker.repository;

import com.yaExpences.yaExpencesTracker.model.SalaryModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SalaryRepository extends JpaRepository<SalaryModel, Integer> {

 SalaryModel findById(int index);


}
