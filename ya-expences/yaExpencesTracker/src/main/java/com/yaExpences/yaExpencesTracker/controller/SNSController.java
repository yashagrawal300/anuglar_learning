package com.yaExpences.yaExpencesTracker.controller;


import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.SubscribeRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("AWS/SNS")
public class SNSController {

    @Autowired
    private AmazonSNSClient snsClient;

    String TOPIC_ARN = "arn:aws:sns:us-east-1:049776452492:yaExpense";

    @GetMapping("/addSubscription/{email}")
    public ResponseEntity addSubscription(@PathVariable String email){
        SubscribeRequest subscribeRequest = new SubscribeRequest(TOPIC_ARN, "email", email);
        snsClient.subscribe(subscribeRequest);
        return new ResponseEntity("Subscription request is pending. To confiem the subscription, check your email", HttpStatus.OK);
    }

    @GetMapping("/sendNotificationToAll/{subject}/{message}")
    public ResponseEntity sendNotifictionToAll(@PathVariable String subject, @PathVariable String message){
        PublishRequest publishRequest = new PublishRequest(TOPIC_ARN, message, subject);
        snsClient.publish(publishRequest);
        return new ResponseEntity(HttpStatus.OK);

    }



}
