package com.yaExpences.yaExpencesTracker.model;

public class KafkaModel {
    public int number;
    public String message;

    public KafkaModel() {

    }

    public KafkaModel(int number, String message) {
        this.number = number;
        this.message = message;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "KafkaModel{" +
                "number=" + number +
                ", message='" + message + '\'' +
                '}';
    }
}
