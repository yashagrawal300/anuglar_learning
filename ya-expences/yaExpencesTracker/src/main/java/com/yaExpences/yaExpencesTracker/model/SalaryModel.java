package com.yaExpences.yaExpencesTracker.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "salary")
public class SalaryModel {
    @Id
    private int index;
    private int moneyLeft;
    private int spentAmount;
    private Date date;


    public SalaryModel(){

    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public SalaryModel(int id, int moneyLeft, int spentAmount, Date date) {
        this.index = id;
        this.moneyLeft = moneyLeft;
        this.spentAmount = spentAmount;
        this.date = date;

    }

    public int getId() {
        return index;
    }

    public void setId(int id) {
        this.index = id;
    }

    public int getMoneyLeft() {
        return moneyLeft;
    }

    public void setMoneyLeft(int moneyLeft) {
        this.moneyLeft = moneyLeft;
    }

    public int getSpentAmount() {
        return spentAmount;
    }

    public void setSpentAmount(int spentAmount) {
        this.spentAmount = spentAmount;
    }
}
