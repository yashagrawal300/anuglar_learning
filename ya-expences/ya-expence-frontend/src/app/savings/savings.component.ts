import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../api-service.service';
import * as echarts from 'echarts';

@Component({
  selector: 'app-savings',
  templateUrl: './savings.component.html',
  styleUrls: ['./savings.component.css']
})
export class SavingsComponent implements OnInit {

  constructor(public apiService: ApiServiceService) {}
  SavingData: any;
  monthData:any= [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ]
  savingData:any = [0,0,0,0,0,0,0,0,0,0,0,0];
  expenseData: any = [0,0,0,0,0,0,0,0,0,0,0,0];
  totalSaving = 0;
  totalSpent:any = 0;
  MonthName(num:string){
    return this.monthData[parseInt(num) -1]
  }


  ngOnInit(): void {

    this.apiService.getAllSalary().subscribe((data:any)=>{
      this.SavingData = data;
      for(let i=0; i <data.length;i++){
        this.totalSaving += parseInt(data[i]["moneyLeft"])
        this.totalSpent += parseInt(data[i]["spentAmount"])
        this.savingData[data[i]["id"]] = parseInt(data[i]["moneyLeft"])
        this.expenseData[data[i]["id"]] = parseInt(data[i]["spentAmount"])

      }


      //chart Code
    type EChartsOption = echarts.EChartsOption;

    var chartDom = document.getElementById('main')!;
    var myChart = echarts.init(chartDom, 'dark');
    var option: EChartsOption;
    
    option = {
      animationDuration: 2000,
      tooltip: {
        trigger: 'axis'
      },
      legend: {
        data: ['Savings', "Expense"]
      },
      grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
      },
      toolbox: {
        feature: {
          saveAsImage: {}
        }
      },
      xAxis: {
        type: 'category',
        boundaryGap: false,
        data: this.monthData
      },
      yAxis: {
        type: 'value'
      },
      series: [

        {
          name: 'Savings',
          type: 'bar',
          itemStyle: {
            borderRadius: 30,
            color: new echarts.graphic.LinearGradient(1, 0, 0, 1, [
              { offset: 0, color: 'yellow' },
              { offset: 1, color: 'green' }

            ])
          },
          data: this.savingData
        },
        {
          name: 'Expense',
          type: 'bar',
          itemStyle: {
            borderRadius: 30,
            color: new echarts.graphic.LinearGradient(1, 0, 0, 1, [
              { offset: 0, color: 'red' },
              { offset: 1, color: 'blue' }

            ])
          },
          data: this.expenseData
        }
        
        
      ]
    };
    
    
    option && myChart.setOption(option);

    })

  }

}
