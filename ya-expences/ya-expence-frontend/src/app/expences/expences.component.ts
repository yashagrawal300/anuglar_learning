import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../api-service.service';
import * as echarts from 'echarts';

@Component({
  selector: 'app-expences',
  templateUrl: './expences.component.html',
  styleUrls: ['./expences.component.css']
})
export class ExpencesComponent implements OnInit {
  expenseThisMonth:any;
  forDate1:any;
  forDate2:any;
  totalExpense:number = 0;
  //chart Data
category:any = [];
barData:any = [];



  renderChart(){
    

      type EChartsOption = echarts.EChartsOption;

      var chartDom = document.getElementById('main')!;
      var myChart = echarts.init(chartDom);
      var option: EChartsOption;
      

      // Generate data

      // option
      option = {
        animationDuration: 5000,
        backgroundColor: '#000000',
        tooltip: {
          trigger: 'axis',
          axisPointer: {
            type: 'shadow'
          }
        },
        legend: {
          data: ['line', 'bar'],
          textStyle: {
            color: '#ccc'
          }
        },
        xAxis: {
          data: this.category,
          axisLine: {
            lineStyle: {
              color: '#ccc'
            }
          }
        },
        yAxis: {
          splitLine: { show: false },
          axisLine: {
            lineStyle: {
              color: '#ccc'
            }
          }
        },
        series: [
          {
            name: 'line',
            type: 'line',
            smooth: true,
            showAllSymbol: true,
            symbol: 'emptyCircle',
            symbolSize: 10,
          color: "yellow",
            data: this.barData
          },
          {
            name: 'bar',
            type: 'bar',
            barWidth: 30,
            itemStyle: {
              borderRadius: 10,
              color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                { offset: 0, color: 'blue' },
                { offset: 1, color: 'red' }

              ])
            },
            data: this.barData
          },
        
        ]
      };

      option && myChart.setOption(option);
  


  }

  
  

  getExpense(){
    

    this.apiService.getTodayExpense(this.forDate1, this.forDate2).subscribe(data=>{
      this.expenseThisMonth = data;
      this.totalExpense = 0;
      this.barData = []
      this.category = []

      this.expenseThisMonth.map((d:any)=>{
        this.totalExpense+=d.spent;

        //Chart date order
    
        if(this.barData.length !==0){
          if(this.category[this.category.length -1] === d.exdate.slice(0, 10)){
            this.barData[this.barData.length-1] += d.spent;
      
          } 
          else{
            this.barData.push(d.spent)
            this.category.push(d.exdate.slice(0, 10))
          }

        }
        else{
        this.barData.push(d.spent)
        this.category.push(d.exdate.slice(0, 10))

        }

        //End of Chart date order

      })

      this.renderChart();

    })

  }


  constructor(public apiService:ApiServiceService) { }

  ngOnInit(): void {
    const date = new Date();
    var date2:string = "";
    var date1:string = "";
    const year = date.getFullYear();
    var month:any = date.getMonth()+1;
    var da:any = date.getDate();

    month = month < 10 ? '0'+month:month;
    da = da < 10 ? '0'+da:da;

    date2 = year + "-" + month + "-" + da;
    date1 = year+ "-"+month+"-"+"01";
  
    this.forDate1 = date1;
    this.forDate2 = date2;


    this.apiService.getTodayExpense(date1, date2).subscribe(data=>{
      this.expenseThisMonth = data;
      this.expenseThisMonth.map((d:any)=>{
        this.totalExpense+=d.spent;

        //Chart date order

        if(this.barData.length !==0){
          if(this.category[this.category.length -1] === d.exdate.slice(0, 10)){
            this.barData[this.barData.length-1] += d.spent;
            
          } 
          else{

            this.barData.push(d.spent)

        this.category.push(d.exdate.slice(0, 10))
          }

        }
        else{

        this.barData.push(d.spent)

        this.category.push(d.exdate.slice(0, 10))

        }

        //End of Chart date order

      })

      this.renderChart();

      
    })

  }

}
