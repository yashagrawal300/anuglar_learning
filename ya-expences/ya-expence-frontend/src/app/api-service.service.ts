import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http"
@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {
  spentData: any;
  totalSaving: number=0;
  savingIndex: any;
  baseURL:string = "http://localhost:8080";




  constructor(private http:HttpClient) { }



  updateSalary(salary:any){
    let url:string = this.baseURL+"/api/updateSalary";
    const headers = { 'content-type': 'application/json'}  
    const data = {
      "moneyLeft": salary,
      "id":this.savingIndex
    }

    return this.http.put(url, data, {'headers':headers})

  }

  addExpense(data:any){
    let url:string = this.baseURL+"/api/addExpence"
    const headers = { 'content-type': 'application/json'}  


    return this.http.post(url, data, {'headers':headers} )

  }

  getAllSalary(){
    let url:string = this.baseURL+"/api/getAllSalary";
    return this.http.get(url)
  }

  getTotalSaving(){
    const headers = { 'content-type': 'application/json'}  
    let url:string = this.baseURL+ "/api/getCurrentMonthSalary"
    return this.http.get(url, {'headers':headers});

  }

  getTodayExpense(date1:string, date2:string){
    
    let url:string = this.baseURL+"/api/getExpences/"+date1+"/"+date2;
    console.log(url);
    return this.http.get(url);

  }

}
