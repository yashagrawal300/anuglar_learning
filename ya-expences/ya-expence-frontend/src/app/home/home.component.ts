import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../api-service.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  //Get the last saving from the database
  //Get todays date and get expenseData data from the database
  saving:any = 50000;
  expenses: string = "";
  allTags:string[] = ["Food", "Movie", "SIP", "Amazon", "WiFi", "Phone Recharge", "Grocery", "Snacks"]
  spentAmount:any = 0;
  expenseData: any;
  totalSpent:any = 0;



  constructor(public apiSerivce: ApiServiceService) { }

  addTag(tag:string):void{
    this.expenses = this.expenses +" "+ tag;
  }

  getExpense(){
    const date = new Date();
    var dateString:string = "";
    const year = date.getFullYear();
    var month:any = date.getMonth()+1;
    var da:any = date.getDate();

    month = month < 10 ? '0'+month:month;
    da = da < 10 ? '0'+da:da;

    dateString = year + "-" + month + "-" + da;

    this.apiSerivce.getTodayExpense(dateString, dateString).subscribe(data=>{
      this.expenseData = data;

    })
  }



  updateSalary(){
    this.apiSerivce.updateSalary(this.saving).subscribe(data=>{
      console.log(data);
    })
  }


  addExpense():void{

    if(this.spentAmount !== 0 && this.expenses !==""){

      this.saving = this.saving - this.spentAmount;
      this.totalSpent = parseInt(this.totalSpent) + parseInt(this.spentAmount);
      const data = {
        "expence": this.expenses,
        "spent": this.spentAmount
      }
      this.expenses = "";
      this.spentAmount = 0;

      this.apiSerivce.addExpense(data).subscribe(data=>{
        this.getExpense();
      })

    }


  }




  ngOnInit(): void {
    this.apiSerivce.getTotalSaving().subscribe((data:any)=>{
      this.saving = data.moneyLeft;
      this.totalSpent = data.spentAmount;
      this.apiSerivce.savingIndex = data.id;
      
    })
    const date = new Date();
    var dateString:string = "";
    const year = date.getFullYear();
    var month:any = date.getMonth()+1;
    var da:any = date.getDate();

    month = month < 10 ? '0'+month:month;
    da = da < 10 ? '0'+da:da;

    dateString = year + "-" + month + "-" + da;


    this.apiSerivce.getTodayExpense(dateString, dateString).subscribe(data=>{
      this.expenseData = data;
      console.log(data);
    })

  }

}