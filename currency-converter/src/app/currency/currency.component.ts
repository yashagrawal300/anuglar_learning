import { Component, OnInit } from '@angular/core';
import { ApiserviceService } from '../apiservice.service';

@Component({
  selector: 'app-currency',
  templateUrl: './currency.component.html',
  styleUrls: ['./currency.component.css']
})
export class CurrencyComponent implements OnInit {
  inputCurr: string = "inr";
  outputCurr: string = "inr";
  inRupee: number = 0;
  apiData = [];


  inpCurr: number = 0;
  outCurr: number = 0;

  giveChange(): void{
    switch(this.inputCurr){
      case 'usd':
        this.inRupee = 74.77*this.inpCurr;
        break;
      case 'euro':
        this.inRupee = 84.34*this.inpCurr;
        break;
      case 'dinar':
        this.inRupee = 247.5*this.inpCurr;
        break;
      case 'cd':
        this.inRupee = 59.11*this.inpCurr;
        break;
      default:
        this.inRupee = this.inpCurr;
        break;
    }


    switch(this.outputCurr){
      case "usd":
        this.outCurr=this.inRupee/74.77;
        break;
      case "euro":
        this.outCurr =this.inRupee/84.34;
        break;
      case "dinar":
        this.outCurr = this.inRupee/247.5;
        break;
      case "cd":
        this.outCurr = this.inRupee/59.11;
        break;
      default:
        this.outCurr=this.inRupee;
        break
    }

  }

  constructor(public apiservice: ApiserviceService) { }

  logthis(): void{
    this.apiservice.logging("currency");
    this.apiservice.data = 3;
    console.log(this.apiservice.data);
    this.apiData = this.apiservice.apiData;
    

  }

  ngOnInit(): void {
  }

}
