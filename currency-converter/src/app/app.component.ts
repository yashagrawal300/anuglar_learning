import { Component } from '@angular/core';
import { ApiserviceService } from './apiservice.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'currency-converter';
  constructor(public apiservice: ApiserviceService){}
  

  logFromParent(): void{
    this.apiservice.logging("parent");
    console.log(this.apiservice.data);
    this.apiservice.getData().subscribe(data=>{
      this.apiservice.apiData = data;
      
    });

  }




}
