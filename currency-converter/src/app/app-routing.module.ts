import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CurrencyComponent } from './currency/currency.component';
import { TableViewerComponent } from './table-viewer/table-viewer.component';
const route: Routes = [
  {path: 'CC', component: CurrencyComponent},
  {path: "TV", component: TableViewerComponent}
];



@NgModule({
  imports: [RouterModule.forRoot(route)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
