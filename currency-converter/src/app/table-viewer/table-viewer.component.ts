import { Component, OnInit } from '@angular/core';
import { ApiserviceService } from '../apiservice.service';
@Component({
  selector: 'app-table-viewer',
  templateUrl: './table-viewer.component.html',
  styleUrls: ['./table-viewer.component.css']
})
export class TableViewerComponent implements OnInit {

  constructor(public apiservice: ApiserviceService) { }

  logthis(): void{
    this.apiservice.logging("currency");
    this.apiservice.data = 3;
    console.log(this.apiservice.data);
    
  }

  ngOnInit(): void {
  }

}
