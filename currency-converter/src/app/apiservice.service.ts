import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ApiserviceService {
  data: number = 1;
  apiData: any;




  logging(str: string):void{
    console.log("logging this " + str);

  }

  constructor(private http:HttpClient) { }

  getData(){
    let url: string = "https://jsonplaceholder.typicode.com/posts";
    return this.http.get(url);

  }


}
